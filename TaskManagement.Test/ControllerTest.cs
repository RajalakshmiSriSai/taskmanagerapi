﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using TaskManagement.Entity;
using TaskManagement.Api;
using System.Web.Http.Cors;
using TaskManagement.Api.Controllers;
using NUnit.Framework;
using TaskManagement.TestHelp;
using TaskManagement.Entity;
namespace TaskManagement.Test
{
    [TestFixture]
    public class ControllerTest
    {
        TasksController _controller;
        private ITaskRepositry _TaskRep;

        [SetUp]
        public void SetUp()
        {
          
            _TaskRep = new TestRepositoryFakeService();
            _controller = new TasksController(_TaskRep);
            _controller.Configuration = new System.Web.Http.HttpConfiguration();
            _controller.Request = new System.Net.Http.HttpRequestMessage();

        }

        [Test]
        public void When_TaskById_ThenOkResult()
        {
            var response = _controller.GetTaskById(1);
            NUnit.Framework.Assert.IsNotNull(response);
            var result = response.ExecuteAsync(new System.Threading.CancellationToken()).GetAwaiter().GetResult();
            NUnit.Framework.Assert.IsNotNull(result.Content);
            NUnit.Framework.Assert.IsTrue(result.IsSuccessStatusCode);
            NUnit.Framework. Assert.AreEqual(System.Net.HttpStatusCode.OK, result.StatusCode);

        }


        [Test]
        public void When_GetAllTask_ThenOkResult()
        {
            var response = _controller.GetAllTask();
            NUnit.Framework.Assert.IsNotNull(response);
            var result = response.ExecuteAsync(new System.Threading.CancellationToken()).GetAwaiter().GetResult();
            NUnit.Framework.Assert.IsNotNull(result.Content);
            NUnit.Framework.Assert.IsTrue(result.IsSuccessStatusCode);
            NUnit.Framework.Assert.AreEqual(System.Net.HttpStatusCode.OK, result.StatusCode);

        }

        [Test]
        public void When_AddNewTaskItem_ThenOkResult()
        {
            var task = new Entity.Task()
            {
                TaskId = 6,
                TaskName = "Task6",
                Priority = 3,
                EndDate = DateTime.Now.Date,
                StartDate = DateTime.Now.Date
            };
            var response = _controller.AddTask(task);
            NUnit.Framework.Assert.IsNotNull(response);
            var result = response.ExecuteAsync(new System.Threading.CancellationToken()).GetAwaiter().GetResult();
            NUnit.Framework.Assert.IsNotNull(result);
            NUnit.Framework.Assert.IsTrue(result.IsSuccessStatusCode);
        }

        [Test]
        public void When_UpdateTask_ThenOkResult()
        {
            var task = new Entity.Task()
            {
                TaskId = 1,
                TaskName = "Task6",
                Priority = 3,
                EndDate = DateTime.Now.Date,
                StartDate = DateTime.Now.Date
            };
            var response = _controller.UpdateTask(task);
            NUnit.Framework.Assert.IsNotNull(response);
            var result = response.ExecuteAsync(new System.Threading.CancellationToken()).GetAwaiter().GetResult();
            NUnit.Framework.Assert.IsNotNull(result);
            NUnit.Framework.Assert.IsTrue(result.IsSuccessStatusCode);

        }

        [Test]
        public void When_EndTask_ThenOkResult()
        {
            var response = _controller.EndTask(2);
            NUnit.Framework.Assert.IsNotNull(response);
            var result = response.ExecuteAsync(new System.Threading.CancellationToken()).GetAwaiter().GetResult();
            NUnit.Framework.Assert.IsNotNull(result.Content);
            NUnit.Framework.Assert.IsTrue(result.IsSuccessStatusCode);
            NUnit.Framework.Assert.AreEqual(System.Net.HttpStatusCode.OK, result.StatusCode);

        }

        [Test]
        public void When_DeleteTask_ThenOkResult()
        {
            var response = _controller.DeleteTask(3);
            NUnit.Framework.Assert.IsNotNull(response);
            var result = response.ExecuteAsync(new System.Threading.CancellationToken()).GetAwaiter().GetResult();
            NUnit.Framework.Assert.IsNotNull(result.Content);
            NUnit.Framework.Assert.IsTrue(result.IsSuccessStatusCode);
            NUnit.Framework.Assert.AreEqual(System.Net.HttpStatusCode.OK, result.StatusCode);

        }

    }
}
