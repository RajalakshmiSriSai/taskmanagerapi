﻿# TaskManagement.Performance.Testing.PerformanceTesting+GetAllTasks_ThroughputMode
_12/18/2018 2:22:24 PM_
### System Info
```ini
NBench=NBench, Version=1.2.2.0, Culture=neutral, PublicKeyToken=null
OS=Microsoft Windows NT 6.2.9200.0
ProcessorCount=4
CLR=4.0.30319.42000,IsMono=False,MaxGcGeneration=2
```

### NBench Settings
```ini
RunMode=Throughput, TestMode=Test
NumberOfIterations=1, MaximumRunTime=00:00:01
Concurrent=False
Tracing=False
```

## Data
-------------------

### Totals
|          Metric |           Units |             Max |         Average |             Min |          StdDev |
|---------------- |---------------- |---------------- |---------------- |---------------- |---------------- |
|    Elapsed Time |              ms |          984.00 |          984.00 |          984.00 |            0.00 |
|[Counter] GetAllTasksCounter |      operations |    8,781,500.00 |    8,781,500.00 |    8,781,500.00 |            0.00 |

### Per-second Totals
|          Metric |       Units / s |         Max / s |     Average / s |         Min / s |      StdDev / s |
|---------------- |---------------- |---------------- |---------------- |---------------- |---------------- |
|    Elapsed Time |              ms |        1,000.22 |        1,000.22 |        1,000.22 |            0.00 |
|[Counter] GetAllTasksCounter |      operations |    8,926,288.87 |    8,926,288.87 |    8,926,288.87 |            0.00 |

### Raw Data
#### Elapsed Time
|           Run # |              ms |          ms / s |         ns / ms |
|---------------- |---------------- |---------------- |---------------- |
|               1 |          984.00 |        1,000.22 |      999,775.91 |

#### [Counter] GetAllTasksCounter
|           Run # |      operations |  operations / s | ns / operations |
|---------------- |---------------- |---------------- |---------------- |
|               1 |    8,781,500.00 |    8,926,288.87 |          112.03 |


## Benchmark Assertions

* [PASS] Expected [Counter] GetAllTasksCounter to must be greater than or equal to 500.00 operations; actual value was 8,926,288.87 operations.
* [PASS] Expected Elapsed Time to must be less than or equal to 600,000.00 ms; actual value was 984.00 ms.

