﻿# TaskManagement.Performance.Testing.PerformanceTesting+GetAllTasks_ThroughputMode
_12/18/2018 2:44:01 PM_
### System Info
```ini
NBench=NBench, Version=1.2.2.0, Culture=neutral, PublicKeyToken=null
OS=Microsoft Windows NT 6.2.9200.0
ProcessorCount=4
CLR=4.0.30319.42000,IsMono=False,MaxGcGeneration=2
```

### NBench Settings
```ini
RunMode=Throughput, TestMode=Test
NumberOfIterations=1, MaximumRunTime=00:00:01
Concurrent=False
Tracing=False
```

## Data
-------------------

### Totals
|          Metric |           Units |             Max |         Average |             Min |          StdDev |
|---------------- |---------------- |---------------- |---------------- |---------------- |---------------- |
|    Elapsed Time |              ms |          997.00 |          997.00 |          997.00 |            0.00 |
|[Counter] GetAllTasksCounter |      operations |    8,720,000.00 |    8,720,000.00 |    8,720,000.00 |            0.00 |

### Per-second Totals
|          Metric |       Units / s |         Max / s |     Average / s |         Min / s |      StdDev / s |
|---------------- |---------------- |---------------- |---------------- |---------------- |---------------- |
|    Elapsed Time |              ms |        1,000.36 |        1,000.36 |        1,000.36 |            0.00 |
|[Counter] GetAllTasksCounter |      operations |    8,749,390.95 |    8,749,390.95 |    8,749,390.95 |            0.00 |

### Raw Data
#### Elapsed Time
|           Run # |              ms |          ms / s |         ns / ms |
|---------------- |---------------- |---------------- |---------------- |
|               1 |          997.00 |        1,000.36 |      999,639.72 |

#### [Counter] GetAllTasksCounter
|           Run # |      operations |  operations / s | ns / operations |
|---------------- |---------------- |---------------- |---------------- |
|               1 |    8,720,000.00 |    8,749,390.95 |          114.29 |


## Benchmark Assertions

* [PASS] Expected [Counter] GetAllTasksCounter to must be greater than or equal to 500.00 operations; actual value was 8,749,390.95 operations.
* [PASS] Expected Elapsed Time to must be less than or equal to 600,000.00 ms; actual value was 997.00 ms.

