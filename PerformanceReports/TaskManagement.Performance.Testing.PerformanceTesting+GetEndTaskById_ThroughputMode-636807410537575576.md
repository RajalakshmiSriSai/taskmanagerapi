﻿# TaskManagement.Performance.Testing.PerformanceTesting+GetEndTaskById_ThroughputMode
_12/18/2018 2:44:13 PM_
### System Info
```ini
NBench=NBench, Version=1.2.2.0, Culture=neutral, PublicKeyToken=null
OS=Microsoft Windows NT 6.2.9200.0
ProcessorCount=4
CLR=4.0.30319.42000,IsMono=False,MaxGcGeneration=2
```

### NBench Settings
```ini
RunMode=Throughput, TestMode=Test
NumberOfIterations=1, MaximumRunTime=00:00:01
Concurrent=False
Tracing=False
```

## Data
-------------------

### Totals
|          Metric |           Units |             Max |         Average |             Min |          StdDev |
|---------------- |---------------- |---------------- |---------------- |---------------- |---------------- |
|    Elapsed Time |              ms |        1,016.00 |        1,016.00 |        1,016.00 |            0.00 |
|[Counter] EndTaskCounter |      operations |    2,440,500.00 |    2,440,500.00 |    2,440,500.00 |            0.00 |

### Per-second Totals
|          Metric |       Units / s |         Max / s |     Average / s |         Min / s |      StdDev / s |
|---------------- |---------------- |---------------- |---------------- |---------------- |---------------- |
|    Elapsed Time |              ms |        1,000.19 |        1,000.19 |        1,000.19 |            0.00 |
|[Counter] EndTaskCounter |      operations |    2,402,523.08 |    2,402,523.08 |    2,402,523.08 |            0.00 |

### Raw Data
#### Elapsed Time
|           Run # |              ms |          ms / s |         ns / ms |
|---------------- |---------------- |---------------- |---------------- |
|               1 |        1,016.00 |        1,000.19 |      999,810.14 |

#### [Counter] EndTaskCounter
|           Run # |      operations |  operations / s | ns / operations |
|---------------- |---------------- |---------------- |---------------- |
|               1 |    2,440,500.00 |    2,402,523.08 |          416.23 |


## Benchmark Assertions

* [PASS] Expected [Counter] EndTaskCounter to must be greater than or equal to 500.00 operations; actual value was 2,402,523.08 operations.
* [PASS] Expected Elapsed Time to must be less than or equal to 600,000.00 ms; actual value was 1,016.00 ms.

