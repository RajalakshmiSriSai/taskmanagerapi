﻿# TaskManagement.Performance.Testing.PerformanceTesting+GetTasksById_ThroughputMode
_12/18/2018 2:44:27 PM_
### System Info
```ini
NBench=NBench, Version=1.2.2.0, Culture=neutral, PublicKeyToken=null
OS=Microsoft Windows NT 6.2.9200.0
ProcessorCount=4
CLR=4.0.30319.42000,IsMono=False,MaxGcGeneration=2
```

### NBench Settings
```ini
RunMode=Throughput, TestMode=Test
NumberOfIterations=1, MaximumRunTime=00:00:01
Concurrent=False
Tracing=False
```

## Data
-------------------

### Totals
|          Metric |           Units |             Max |         Average |             Min |          StdDev |
|---------------- |---------------- |---------------- |---------------- |---------------- |---------------- |
|    Elapsed Time |              ms |          999.00 |          999.00 |          999.00 |            0.00 |
|[Counter] GetTaskByIdCounter |      operations |    5,903,500.00 |    5,903,500.00 |    5,903,500.00 |            0.00 |

### Per-second Totals
|          Metric |       Units / s |         Max / s |     Average / s |         Min / s |      StdDev / s |
|---------------- |---------------- |---------------- |---------------- |---------------- |---------------- |
|    Elapsed Time |              ms |          999.62 |          999.62 |          999.62 |            0.00 |
|[Counter] GetTaskByIdCounter |      operations |    5,907,147.66 |    5,907,147.66 |    5,907,147.66 |            0.00 |

### Raw Data
#### Elapsed Time
|           Run # |              ms |          ms / s |         ns / ms |
|---------------- |---------------- |---------------- |---------------- |
|               1 |          999.00 |          999.62 |    1,000,382.88 |

#### [Counter] GetTaskByIdCounter
|           Run # |      operations |  operations / s | ns / operations |
|---------------- |---------------- |---------------- |---------------- |
|               1 |    5,903,500.00 |    5,907,147.66 |          169.29 |


## Benchmark Assertions

* [PASS] Expected [Counter] GetTaskByIdCounter to must be greater than or equal to 500.00 operations; actual value was 5,907,147.66 operations.
* [PASS] Expected Elapsed Time to must be less than or equal to 600,000.00 ms; actual value was 999.00 ms.

