﻿# TaskManagement.Performance.Testing.PerformanceTesting+GetAllTasks_ThroughputMode
_12/18/2018 2:22:17 PM_
### System Info
```ini
NBench=NBench, Version=1.2.2.0, Culture=neutral, PublicKeyToken=null
OS=Microsoft Windows NT 6.2.9200.0
ProcessorCount=4
CLR=4.0.30319.42000,IsMono=False,MaxGcGeneration=2
```

### NBench Settings
```ini
RunMode=Throughput, TestMode=Test
NumberOfIterations=1, MaximumRunTime=00:00:01
Concurrent=False
Tracing=False
```

## Data
-------------------

### Totals
|          Metric |           Units |             Max |         Average |             Min |          StdDev |
|---------------- |---------------- |---------------- |---------------- |---------------- |---------------- |
|    Elapsed Time |              ms |          989.00 |          989.00 |          989.00 |            0.00 |
|[Counter] GetAllTasksCounter |      operations |    8,604,500.00 |    8,604,500.00 |    8,604,500.00 |            0.00 |

### Per-second Totals
|          Metric |       Units / s |         Max / s |     Average / s |         Min / s |      StdDev / s |
|---------------- |---------------- |---------------- |---------------- |---------------- |---------------- |
|    Elapsed Time |              ms |          999.94 |          999.94 |          999.94 |            0.00 |
|[Counter] GetAllTasksCounter |      operations |    8,699,687.63 |    8,699,687.63 |    8,699,687.63 |            0.00 |

### Raw Data
#### Elapsed Time
|           Run # |              ms |          ms / s |         ns / ms |
|---------------- |---------------- |---------------- |---------------- |
|               1 |          989.00 |          999.94 |    1,000,059.15 |

#### [Counter] GetAllTasksCounter
|           Run # |      operations |  operations / s | ns / operations |
|---------------- |---------------- |---------------- |---------------- |
|               1 |    8,604,500.00 |    8,699,687.63 |          114.95 |


## Benchmark Assertions

* [PASS] Expected [Counter] GetAllTasksCounter to must be greater than or equal to 500.00 operations; actual value was 8,699,687.63 operations.
* [PASS] Expected Elapsed Time to must be less than or equal to 600,000.00 ms; actual value was 989.00 ms.

