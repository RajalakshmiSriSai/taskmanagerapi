﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using TaskManagement.DAL;
using TaskManagement.Entity;
using TaskManagement.BL;
using System.Web.Http.Cors;

namespace TaskManagement.Api.Controllers
{
    [EnableCors(origins: "http://localhost:4200",headers:"*",methods:"*")]
    [RoutePrefix("Api")]
    public class TasksController : ApiController
    {
        private ITaskRepositry taskRep;

       public TasksController()
        {
            taskRep = new TaskRepositry();
        }
        public TasksController(ITaskRepositry taskRep)
        {
            this.taskRep = taskRep;
        }
        // GET: api/Tasks
        [Route("GetAllTask")]
        [HttpGet]
        public IHttpActionResult GetAllTask()
        {
           var tasks = taskRep.GetAllTask();
            return Ok(tasks);
        }
        // GET: api/Tasks/5
        [Route("GetTaskById/{id}")]
        [HttpGet]
        public IHttpActionResult GetTaskById(int id)
        {
            Task tasks = taskRep.GetTaskById(id);
           
            return Ok(tasks);
        }
        // PUT: api/Tasks/5
        [Route("UpdateTask")]
        [ResponseType(typeof(void))]
        [HttpPut]
        public IHttpActionResult UpdateTask(Task task)
        {
            taskRep.UpdateTask( task);
            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Tasks
        [Route("AddTask")]
        [HttpPost]
        [ResponseType(typeof(Task))]
        public IHttpActionResult AddTask(Task task)
        {
            taskRep.AddTask(task);
            return StatusCode(HttpStatusCode.NoContent);
        }

        // DELETE: api/Tasks/5
        [ResponseType(typeof(Task))]
        [Route("EndTask/{id}")]
        [HttpDelete]
        public IHttpActionResult EndTask(int id)
        {
            Task task = taskRep.EndTaskById(id);
            return Ok(task);
        }

        [ResponseType(typeof(Task))]
        [Route("DeleteTask/{id}")]
        [HttpDelete]
        public IHttpActionResult DeleteTask(int id)
        {
 
            Task task = taskRep.DeleteTask(id);
            return Ok(task);
        }

      
    }
}