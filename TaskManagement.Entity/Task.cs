﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TaskManagement.Entity
{

    [Table("table_Task")]
    public class Task
    {
        [Key]
        public int TaskId
        {
            get; set;
        }
        [StringLength(20)]
        public string TaskName
        {
            get; set;
        }


        public Task Parant
        {
            get; set;
        }
        [Column("Parant_ID")]
        public int? ParantId
        {
            get; set;
        }
        public int Priority 
        {
            get; set;
        }

        [Column(TypeName = "Date")]
        public DateTime? StartDate
        {
            get; set;
        }

        public string StartDateString
        {
            get { return StartDate.HasValue ? StartDate.Value.ToString("yyyy-MM-dd") : null; }
        }

        public string EndDateString
        {
            get { return EndDate.HasValue ? EndDate.Value.ToString("yyyy-MM-dd") : null; }
        }

        [Column(TypeName = "Date")]
        public DateTime? EndDate
        {
            get; set;
        }

    }
    }

