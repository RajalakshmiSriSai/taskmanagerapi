﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaskManagement.Entity;

namespace TaskManagement.TestHelp
{
    public class TestRepositoryFakeService : ITaskRepositry
    {
        private readonly List<Entity.Task> _Tasklist;
        public TestRepositoryFakeService()
        {
            _Tasklist = TestHelp.GetAllTask();
        }
        void ITaskRepositry.AddTask(Entity.Task Item)
        {
            _Tasklist.Add(Item);
        }

        Entity.Task ITaskRepositry.DeleteTask(int id)
        {
          Entity.Task oldValue= _Tasklist.FirstOrDefault(x => x.TaskId == id);
            _Tasklist.Remove(oldValue);
            return oldValue;
        }

        Entity.Task ITaskRepositry.EndTaskById(int id)
        {
            Entity.Task oldValue = _Tasklist.FirstOrDefault(x => x.TaskId == id);
            oldValue.EndDate = DateTime.Now.Date;
            return oldValue;
        }

        List<Entity.Task> ITaskRepositry.GetAllTask()
        {
            return _Tasklist;
        }

        Entity.Task ITaskRepositry.GetTaskById(int id)
        {
            Entity.Task Value = _Tasklist.FirstOrDefault(x => x.TaskId == id);
            return Value;
        }

        void ITaskRepositry.UpdateTask(Entity.Task task)
        {
            Entity.Task NewValue = _Tasklist.FirstOrDefault(x => x.TaskId == task.TaskId);
            NewValue.EndDate = DateTime.Now.Date;
        }
    }
}
