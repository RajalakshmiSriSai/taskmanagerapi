﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaskManagement.Entity;

namespace TaskManagement.TestHelp
{
    public static class TestHelp
    {
      public  static List<Entity.Task>  GetAllTask()
        {
            var taskValue = new List<Entity.Task>
            {
               new Entity.Task()
               {
                  TaskId =1,TaskName ="Task1" , Priority = 1, ParantId = null, StartDate = DateTime.Now, EndDate = DateTime.Now ,
               },
                 new Entity.Task()
               {
                  TaskId =2,TaskName ="Task2" , Priority = 3, ParantId = null, StartDate = DateTime.Now, EndDate = DateTime.Now ,
               },
                new Entity.Task()
               {
                  TaskId =1,TaskName ="Task3" , Priority = 10, ParantId = null, StartDate = DateTime.Now, EndDate = DateTime.Now ,
               }
            };
            return taskValue;
        }
    }
}
