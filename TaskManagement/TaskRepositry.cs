﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaskManagement.DAL;
using TaskManagement.Entity;
using System.ComponentModel.DataAnnotations ;
using System.ComponentModel.DataAnnotations.Schema;
namespace TaskManagement.BL
{
    public class TaskRepositry : ITaskRepositry
    {

        TaskManagerDBContext db;
        public TaskRepositry()
        {
            db = new TaskManagerDBContext();
        }
        public void AddTask(Entity.Task Iteam)
        {
            db.Tasks.Add(Iteam);
            db.SaveChanges();
        }

        public List<Entity.Task> GetAllTask()
        {
            return db.Tasks.ToList();
        }

        public Entity.Task GetTaskById(int id)
        {
            Entity.Task task = db.Tasks.Where(x => x.TaskId == id).SingleOrDefault();
            return task;
        }

        public void UpdateTask(Entity.Task task)
        {
            db.Entry(task).State = System.Data.Entity.EntityState.Modified;
            Entity.Task oldtask = db.Tasks.Where(x => x.TaskId == task.TaskId).SingleOrDefault();
            if (oldtask != null)
            {
                oldtask.TaskId = task.TaskId;
                oldtask.TaskName = task.TaskName;
                oldtask.Priority = task.Priority;
                oldtask.ParantId = task.ParantId;
                oldtask.StartDate = task.StartDate;
                oldtask.EndDate = task.EndDate;
                db.SaveChanges();
             }
        }

        public Entity.Task EndTaskById(int id)
        {
            Entity.Task oldtask = db.Tasks.Where(x => x.TaskId == id).SingleOrDefault();
            if (oldtask != null)
            {
                oldtask.EndDate = DateTime.Now;
                db.SaveChanges();
            }
            return oldtask;
        }

        public Entity.Task DeleteTask(int id)
        {
            Entity.Task oldtask = db.Tasks.Where(x => x.TaskId == id).SingleOrDefault();
            db.Tasks.Remove(oldtask);
            db.SaveChanges();
            return oldtask;
        }
    }
}
