﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskManagement
{
    public interface ITaskRepositry
    {
        void AddTask(Entity.Task Iteam);
        List<Entity.Task> GetAllTask();

        Entity.Task GetTaskById(int id);

        void UpdateTask(Entity.Task task);

         Entity.Task EndTaskById(int id);

         Entity.Task DeleteTask(int id);
    }
}
