﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NBench;
using TaskManagement.BL;
using TaskManagement.TestHelp;
using TaskManagement.Api;
using TaskManagement.Api.Controllers;

namespace TaskManagement.Performance.Testing
{
    public class PerformanceTesting
    {
        private const string GetAllTasksCounter = "GetAllTasksCounter";
        private const string GetTaskByIdCounter = "GetTaskByIdCounter";
        private const string AddTaskCounter = "AddTaskCounter";
        private const string UpdateTaskCounter = "UpdateTaskCounter";
        private const string EndTaskCounter = "EndTaskCounter";
        private const string DeleteTaskCounter = "DeleteTaskCounter";

        private Counter allTasksCounter;
        private Counter taskByIdCounter;
        private Counter addTaskCounter;
        private Counter updateTaskCounter;
        private Counter endTaskCounter;
        private Counter deleteTaskCounter;

        private const int AcceptableMinThroughput = 500;
        private ITaskRepositry taskRepository;

        [PerfSetup]
        public void Setup()
        {
            taskRepository = new TestRepositoryFakeService();
        }

        [PerfBenchmark(RunMode = RunMode.Throughput, TestMode = TestMode.Test, NumberOfIterations = 1)]
        [CounterThroughputAssertion(GetAllTasksCounter, MustBe.GreaterThanOrEqualTo, AcceptableMinThroughput)]
        [ElapsedTimeAssertion(MaxTimeMilliseconds = 600000)]
        public void GetAllTasks_ThroughputMode(BenchmarkContext context)
        {
            //Arrange
            allTasksCounter = context.GetCounter(GetAllTasksCounter);

            //Act
            for (int i = 0; i < 500; i++)
            {
                var controller = new TasksController(taskRepository);
                dynamic result = controller.GetAllTask();

                allTasksCounter.Increment();
            }
        }

        [PerfBenchmark(RunMode = RunMode.Throughput, TestMode = TestMode.Test, NumberOfIterations = 1)]
        [CounterThroughputAssertion(GetTaskByIdCounter, MustBe.GreaterThanOrEqualTo, AcceptableMinThroughput)]
        [ElapsedTimeAssertion(MaxTimeMilliseconds = 600000)]
        public void GetTasksById_ThroughputMode(BenchmarkContext context)
        {
            //Arrange
            taskByIdCounter = context.GetCounter(GetTaskByIdCounter);

            //Act
            for (int i = 0; i < 500; i++)
            {
                var controller = new TasksController(taskRepository);
                dynamic result = controller.GetTaskById(1);

                taskByIdCounter.Increment();
            }
        }
        [PerfBenchmark(RunMode = RunMode.Throughput, TestMode = TestMode.Test, NumberOfIterations = 1)]
        [CounterThroughputAssertion(EndTaskCounter, MustBe.GreaterThanOrEqualTo, AcceptableMinThroughput)]
        [ElapsedTimeAssertion(MaxTimeMilliseconds = 600000)]
        public void GetEndTaskById_ThroughputMode(BenchmarkContext context)
        {
            endTaskCounter = context.GetCounter(EndTaskCounter);

            //Act
            for (int i = 0; i < 500; i++)
            {
                var controller = new TasksController(taskRepository);
                dynamic result = controller.EndTask(1);

                endTaskCounter.Increment();
            }

        }
    }
}
